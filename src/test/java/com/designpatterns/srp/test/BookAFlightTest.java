package com.designpatterns.srp.test;

import com.designpatterns.strategyPattern.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.collections.Maps;

import java.util.HashMap;
import java.util.Map;

public class BookAFlightTest extends BaseTest {

    //strategy

    private BookAFlightPage bookAFlightPage;

    @BeforeTest
    public void setBookAFlightPage() {
        this.bookAFlightPage = new BookAFlightPage(this.driver);
    }

    @Test(dataProvider = "getData")
    public void bookAFlightTest(SearchOption searchOption, Map<String, String> searchDetails) {
        this.bookAFlightPage.goTo();
        this.bookAFlightPage.setSearchOption(searchOption);
        this.bookAFlightPage.getSearchOption().enterSearchInformation(searchDetails);
        this.bookAFlightPage.getSearch().searchFlight();
        System.out.println("Strategy Pattern");
    }

    // if using factory with strategy , object is supplied by factory and map via string
    // so no need to pass object as parameter SearchOption searchOption
    @Test(dataProvider = "getDatadata")
    public void bookAFlightTesttest(String searchOption, Map<String, String> searchDetails) {
        this.bookAFlightPage.goTo();
        this.bookAFlightPage.setSearchOption(SearchOptionFactory.get(searchOption));
        this.bookAFlightPage.getSearchOption().enterSearchInformation(searchDetails);
        this.bookAFlightPage.getSearch().searchFlight();
        System.out.println("Strategy Pattern");
    }

    // strategy
    @DataProvider
    public Object[][] getData() {

        Map<String, String> returnSearchData = Maps.newHashMap();
        returnSearchData.put("departureAirport", "Hyderabad");
        returnSearchData.put("arrivalAirport", "Stockholm");
        returnSearchData.put("departing", "25 Sep 21");
        returnSearchData.put("returning", "28 Sep 21");

        Map<String, String> oneWaySearchData = Maps.newHashMap();
        oneWaySearchData.put("departureAirport", "Hyderabad");
        oneWaySearchData.put("arrivalAirport", "Stockholm");
        oneWaySearchData.put("departing", "25 Sep 21");

        Map<String, String> advancedSearchData = Maps.newHashMap();
        advancedSearchData.put("departureAirport", "Hyderabad");
        advancedSearchData.put("arrivalAirport", "Stockholm");
        advancedSearchData.put("departing", "25 Sep 21");
        advancedSearchData.put("secondDepartureAirport", "Hyderabad");
        advancedSearchData.put("secondArrivalAirport", "Stockholm");
        advancedSearchData.put("secondDeparting", "25 Sep 21");

        return new Object[][]{
                {new ReturnSearchOption(driver), returnSearchData},
                {new OneWay(driver), oneWaySearchData},
                {new AdvancedSearch(driver), advancedSearchData}
        };
    }
    // Strategy with factory pattern
    // if using factory pattern object is supplied from searchoptionfactory class instead in data provider
    @DataProvider
    public Object[][] getDatadata() {

        Map<String, String> returnSearchData = Maps.newHashMap();
        returnSearchData.put("departureAirport", "Hyderabad");
        returnSearchData.put("arrivalAirport", "Stockholm");
        returnSearchData.put("departing", "25 Sep 21");
        returnSearchData.put("returning", "28 Sep 21");

        Map<String, String> oneWaySearchData = Maps.newHashMap();
        oneWaySearchData.put("departureAirport", "Hyderabad");
        oneWaySearchData.put("arrivalAirport", "Stockholm");
        oneWaySearchData.put("departing", "25 Sep 21");

        Map<String, String> advancedSearchData = Maps.newHashMap();
        advancedSearchData.put("departureAirport", "Hyderabad");
        advancedSearchData.put("arrivalAirport", "Stockholm");
        advancedSearchData.put("departing", "25 Sep 21");
        advancedSearchData.put("secondDepartureAirport", "Hyderabad");
        advancedSearchData.put("secondArrivalAirport", "Stockholm");
        advancedSearchData.put("secondDeparting", "25 Sep 21");

        return new Object[][]{
                {"RR", returnSearchData},
                {"OW", oneWaySearchData},
                {"AS", advancedSearchData}
        };
    }
}
