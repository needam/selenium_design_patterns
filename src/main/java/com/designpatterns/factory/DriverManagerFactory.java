package com.designpatterns.factory;

public class DriverManagerFactory {

    private static DriverManager driverManager;

    public static DriverManager getManager(DriverTypes type){
        switch (type){
            case CHROME:
                driverManager = new ChromeDriverManager();
            break;
            case FIREFOX:
                // driverManager = new FirefoxDriverManager();
                break;
            case IE:
                break;
            case EDGE:
                break;
            case SAFARI:
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);

        }
        return driverManager;
    }
}
