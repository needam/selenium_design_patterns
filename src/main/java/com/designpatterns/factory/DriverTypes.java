package com.designpatterns.factory;

public enum DriverTypes {
    CHROME,
    FIREFOX,
    IE,
    EDGE,
    SAFARI
}
