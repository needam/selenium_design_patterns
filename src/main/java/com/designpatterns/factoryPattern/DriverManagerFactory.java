package com.designpatterns.factoryPattern;

public class DriverManagerFactory {
   private static DriverManager driverManager;

    public static DriverManager getManager(DriverType type) {
        switch (type){
            case CHROME:
               driverManager = new ChromeDriverManager();
                break;
        }
        return driverManager;
    }
}
