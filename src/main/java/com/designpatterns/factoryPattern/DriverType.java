package com.designpatterns.factoryPattern;

public enum DriverType {
    CHROME,
    FIREFOX,
    IE,
    EDGE,
    SAFARI
}
