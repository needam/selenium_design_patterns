package com.designpatterns.srp.result;

import com.designpatterns.srp.common.AbstractSearchComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NavigationBar extends AbstractSearchComponent {

    @FindBy(how = How.ID, using = "hdtb")
    private WebElement bar;

    @FindBy(how = How.LINK_TEXT, using = "News")
    private WebElement news;

    @FindBy(how = How.LINK_TEXT, using = "Videos")
    private WebElement videos;


    public NavigationBar(WebDriver driver) {
        super(driver);
    }

    public void goToNews() {
        news.click();
    }

    public void goToVideos() {
        videos.click();
    }

    @Override
    public boolean isDisplayed() {
        return wait.until((d) -> this.bar.isDisplayed());
    }
}
