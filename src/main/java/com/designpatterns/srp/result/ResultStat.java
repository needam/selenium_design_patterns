package com.designpatterns.srp.result;

import com.designpatterns.srp.common.AbstractSearchComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ResultStat extends AbstractSearchComponent {

    @FindBy(how = How.ID, using = "result-stats")
    private WebElement stat;


    public ResultStat(WebDriver driver) {
        super(driver);
    }

    public String getStat() {
        return stat.getText();
    }

    @Override
    public boolean isDisplayed() {
        return wait.until((d) -> stat.isDisplayed());
    }
}
