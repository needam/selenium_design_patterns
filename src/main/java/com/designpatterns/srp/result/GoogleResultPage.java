package com.designpatterns.srp.result;

import com.designpatterns.srp.common.SearchSuggestions;
import com.designpatterns.srp.common.SearchWidget;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class GoogleResultPage {

    private SearchSuggestions searchSuggestions;
    private SearchWidget searchWidget;
    private NavigationBar navigationBar;
    private ResultStat resultStat;

    public GoogleResultPage(final WebDriver driver) {
        this.navigationBar = PageFactory.initElements(driver, NavigationBar.class);
        // can also call as navigationBar = new NavigationBar(driver);
        searchWidget = PageFactory.initElements(driver, SearchWidget.class);
        searchSuggestions = PageFactory.initElements(driver, SearchSuggestions.class);
        resultStat = PageFactory.initElements(driver, ResultStat.class);
    }

    public SearchWidget getSearchWidget() { return searchWidget; }

    public NavigationBar getNavigationBar() { return navigationBar; }

    public ResultStat getResultStat() { return resultStat; }

    public SearchSuggestions getSearchSuggestions() { return searchSuggestions; }
}
