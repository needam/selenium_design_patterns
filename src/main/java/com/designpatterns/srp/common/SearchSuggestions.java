package com.designpatterns.srp.common;

import com.designpatterns.srp.common.AbstractSearchComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class SearchSuggestions extends AbstractSearchComponent {

    @FindBy(how = How.CSS, using = "ul > li.sbct")
    private List<WebElement> suggestions;

    public SearchSuggestions(WebDriver driver) {
        super(driver);
    }

    public void clickSuggestionByIndex(int index){
        suggestions.get(index - 1).click();
    }

    @Override
    public boolean isDisplayed() {
        return this.wait.until((d) -> this.suggestions.size() > 5);
    }
}
