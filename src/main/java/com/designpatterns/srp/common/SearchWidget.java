package com.designpatterns.srp.common;

import com.designpatterns.srp.common.AbstractSearchComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchWidget extends AbstractSearchComponent {

    @FindBy(how = How.NAME, using = "q")
    private WebElement searchBox;

    public SearchWidget(WebDriver driver) {
       super(driver);
    }

    public void enter(String keyword) throws InterruptedException {
        searchBox.clear();
        for(char ch : keyword.toCharArray()) {
            Thread.sleep(10);
            searchBox.sendKeys(ch+ "");
        }
    }

    @Override
    public boolean isDisplayed() {
//        wait.until(ExpectedConditions.visibilityOf(searchBox));
//        return searchBox.isDisplayed();
        return wait.until((d) -> searchBox.isDisplayed());
    }
}
