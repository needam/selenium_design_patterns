package com.designpatterns.strategyPattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;

public class BookAFlightPage {

    private WebDriver driver;

    //    private Return rr;
    //    private OneWay oneWay;
    //    private AdvancedSearch advancedSearch;
    // insted of calling each search option we can call interface
    private Search search;
    private SearchOption searchOption;

    public BookAFlightPage(final WebDriver driver) {
        this.driver = driver;
    //        rr = PageFactory.initElements(driver, Return.class);
    //        oneWay = PageFactory.initElements(driver, OneWay.class);
    //        advancedSearch = PageFactory.initElements(driver, AdvancedSearch.class);
        search = PageFactory.initElements(driver, Search.class);
    }

    public void goTo() {
        driver.get("https://fly10.emirates.com/CAB/IBE/SearchAvailability.aspx");
    }

    public Search getSearch() {
        return search;
    }

    public void setSearchOption(SearchOption searchOption) {
        this.searchOption = searchOption;
        PageFactory.initElements(driver, this.searchOption);
    }

    public SearchOption getSearchOption() {
        return searchOption;
    }
}
