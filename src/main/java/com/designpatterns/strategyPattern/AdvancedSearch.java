package com.designpatterns.strategyPattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class AdvancedSearch implements SearchOption{

    private WebDriver driver;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ltMultiDest")
    private WebElement advancedSearchRadioButton;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ddlFrom1-suggest")
    private WebElement departureAirport;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ddlTo1-suggest")
    private WebElement arrivalAirport;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_txtD1")
    private WebElement departing;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ddlFrom1-suggest")
    private WebElement secondDepartureAirport;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ddlTo1-suggest")
    private WebElement secondArrivalAirport;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_txtD1")
    private WebElement secondDeparting;

    @FindBy(how = How.CSS, using="#ctl00_c_CtWNW_ddlClass1_chosen > a")
    private WebElement departureClass;

    @FindBy(how = How.CSS, using="#ctl00_c_CtWNW_ddlClass1_chosen > div > ul > li")
    private WebElement departureClassOption;

    @FindBy(how = How.CSS, using="#ctl00_c_CtWNW_ddlClass2_chosen > a")
    private WebElement secondDepartureClass;

    @FindBy(how = How.CSS, using="#ctl00_c_CtWNW_ddlClass2_chosen > div > ul > li")
    private WebElement secondDepartureClassOption;

    public AdvancedSearch(final WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @Override
    public void enterSearchInformation(Map<String, String> searchDetails) {
        advancedSearchRadioButton.click();
        this.departureAirport.sendKeys(searchDetails.get("departureAirport"));
        this.arrivalAirport.sendKeys(searchDetails.get("arrivalAirport"));
        this.departing.sendKeys(searchDetails.get("departing"));
        this.departureClass.click();
        Select select = new Select(departureClassOption);
        select.selectByIndex(2);
        this.secondDepartureAirport.sendKeys(searchDetails.get("secondDepartureAirport"));
        this.secondArrivalAirport.sendKeys(searchDetails.get("secondArrivalAirport"));
        this.secondDeparting.sendKeys(searchDetails.get("secondDeparting"));
        this.secondDepartureClass.click();
        select.selectByIndex(2);
    }
}
