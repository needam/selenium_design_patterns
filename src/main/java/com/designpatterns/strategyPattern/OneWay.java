package com.designpatterns.strategyPattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class OneWay implements SearchOption{

    private WebDriver driver;


    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ltOneway")
    private WebElement oneWayRadioButton;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ddlFrom-suggest")
    private WebElement departureAirport;

    @FindBy(how = How.ID, using="ctl00_c_CtWNW_ddlTo-suggest")
    private WebElement arrivalAirport;

    @FindBy(how = How.ID, using="txtDepartDate")
    private WebElement departing;

    @FindBy(how = How.CSS, using="#ctl00_c_CtWNW_flightClass_chosen > a")
    private WebElement departureClass;

    @FindBy(how = How.CSS, using="#ctl00_c_CtWNW_flightClass_chosen > div > ul > li")
    private WebElement departureClassOption;

    @FindBy(how = How.NAME, using="ctl00$c$CtWNW$chkFlexSearch")
    private WebElement dateFlexibleCheckbox;

    public OneWay(final WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @Override
    public void enterSearchInformation(Map<String, String> searchDetails) {
        oneWayRadioButton.click();
        this.departureAirport.sendKeys(searchDetails.get("departureAirport"));
        this.arrivalAirport.sendKeys(searchDetails.get("arrivalAirport"));
        this.departing.sendKeys(searchDetails.get("departing"));
        this.departureClass.click();
        Select select = new Select(departureClassOption);
        select.selectByIndex(1);
        dateFlexibleCheckbox.click();
    }
}
