package com.designpatterns.strategyPattern;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.Map;

public class Search {


    @FindBy(how = How.ID, using="ctl00_c_IBE_PB_FF")
    private WebElement searchButton;

    public void searchFlight() {
        searchButton.click();
    }
}
