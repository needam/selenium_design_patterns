package com.designpatterns.strategyPattern;

import java.util.Map;

public interface SearchOption {
    void enterSearchInformation(Map<String, String>searchDetails);
}
