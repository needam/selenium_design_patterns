package com.designpatterns.strategyPattern;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class SearchOptionFactory {

    private static WebDriver driver;

    private static final Supplier<SearchOption> RR = () -> new ReturnSearchOption(driver);
    private static final Supplier<SearchOption> OW = () -> new OneWay(driver);
    private  static final Supplier<SearchOption> AS = () -> new AdvancedSearch(driver);

    private static final Map<String, Supplier<SearchOption>> MAP = new HashMap<>();


      static {
          MAP.put("RR", RR);
          MAP.put("OW", OW);
          MAP.put("AS", AS);
      }

      // obj searchoption
    public static SearchOption get(String option) {
        return MAP.get(option).get();
    }
}
